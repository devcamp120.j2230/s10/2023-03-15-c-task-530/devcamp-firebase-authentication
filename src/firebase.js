// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyApjVSJ6aionwKtgr-IP_Kp6zjkeRgxukc",
  authDomain: "devcamp-r30.firebaseapp.com",
  projectId: "devcamp-r30",
  storageBucket: "devcamp-r30.appspot.com",
  messagingSenderId: "212536077217",
  appId: "1:212536077217:web:2f52c410849e3538aaf9e0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;