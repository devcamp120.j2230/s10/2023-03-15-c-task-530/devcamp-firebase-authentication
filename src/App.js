import logo from './logo.svg';
import './App.css';

import auth from './firebase';
import { signInWithPopup, GoogleAuthProvider, signOut, onAuthStateChanged } from 'firebase/auth';
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {
  const [user, setUser] = useState(null);

  const logInGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        setUser(result.user);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  const logOutGoogle = () => {
    signOut(auth)
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      console.log(result);
      setUser(result);
    })
  })

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        { user ?
          <>
            <p>
              Hello, {user.displayName}
            </p>
            <img src={user.photoURL} style={{width: "50px", borderRadius: "50%", marginBottom: "20px"}} alt="Avatar"></img>

            <button onClick={logOutGoogle}>Logout with Google</button>
          </>
          :
          <>
            <p>Please Sign In</p>
            <button onClick={logInGoogle}>Login with Google</button>
          </>
        }
        
      </header>
    </div>
  );
}

export default App;
